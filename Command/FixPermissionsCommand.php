<?php

namespace Leandro\CommandsBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FixPermissionsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('leandro:fix-permissions')
            ->setDescription('Soluciona los problemas en cache y log dirs')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('About to run setfacl on app/cache & app/logs');
        exec('sudo setfacl -Rn -m u:www-data:rwX -m u:`whoami`:rwX app/cache app/logs');
        exec('sudo setfacl -dRn -m u:www-data:rwX -m u:`whoami`:rwX app/cache app/logs');
        $output->writeln('Done.');
    }
}
